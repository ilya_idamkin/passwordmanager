﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PasswordManager.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Password Mannager v.0.5";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Idamkin Ilya";

            return View();
        }

        public ActionResult Info(string message)
        {

            ViewBag.Message = message;

            return View();
        }
    }
}
