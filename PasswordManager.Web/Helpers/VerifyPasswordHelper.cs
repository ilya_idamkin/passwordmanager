﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasswordManager.Web.Helpers
{
    public static class VerifyPasswordHelper
    {
        private const string key = "pass";

        public static void KeepCryptoHash(this Controller controller)
        {
            controller.TempData.Keep(key);
        }

        public static string RetriveCryptoHash(this Controller controller)
        {
            return (string) controller.TempData[key];
        }

        public static void PutCryptoHash(this Controller controller, string password)
        {
            controller.TempData[key] = password;
        }

        public static IActionResult CallVerifyPassword(this Controller controller)
        {
            return controller.RedirectToAction("VerifyPassword", "Account", new { ReturnUrl = UriHelper.GetDisplayUrl(controller.Re‌​quest) });
        }
    }
}
