﻿using System.ComponentModel.DataAnnotations;

namespace PasswordManager.Web.Models.SharedViewModels
{
    public class ErrorViewModel
    {
        [Display(Name = "Error")]
        public string Error { get; set; }

        [Display(Name = "Description")]
        public string ErrorDescription { get; set; }
    }
}
