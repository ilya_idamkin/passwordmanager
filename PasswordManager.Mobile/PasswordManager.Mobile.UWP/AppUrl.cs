﻿using PasswordManager.Mobile;
using PasswordManager.Mobile.UWP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppUrl_UWP))]
namespace PasswordManager.Mobile.UWP
{
    public class AppUrl_UWP : IAppUrl
    {
        public string Get() {
            return "http://192.168.1.101:3000/";
        }
    }
}
