﻿using System;
using System.Collections.Generic;
using System.Text;
using PasswordManager.Core;
using PasswordManager.Core.Models;

namespace PasswordManager.Core.Extensions
{
    public static class CryptographyNoteExtension
    {
        public static INote Encrypt(this INote note, string password)
        {
            if (note.Login != null)
                note.Login = Cryptography.Encrypt(note.Login, password);
            if (note.Password != null)
                note.Password = Cryptography.Encrypt(note.Password, password);
            if (note.Other != null)
                note.Other = Cryptography.Encrypt(note.Other, password);
            return note;
        }

        public static INote Decrypt(this INote note, string password)
        {
            if (note.Login != null)
                note.Login = Cryptography.Decrypt(note.Login, password);
            if (note.Password != null)
                note.Password = Cryptography.Decrypt(note.Password, password);
            if (note.Other != null)
                note.Other = Cryptography.Decrypt(note.Other, password);
            return note;
        }
    }
}
