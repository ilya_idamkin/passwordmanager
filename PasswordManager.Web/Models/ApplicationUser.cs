﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasswordManager.Web.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ICollection<Note> Notes { get; set; }

    }
}
