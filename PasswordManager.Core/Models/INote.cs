﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PasswordManager.Core.Models
{
    public interface INote
    {
        int Id { get; set; }
        string Name { get; set; }
        string Login { get; set; }
        string Password { get; set; }
        string Other { get; set; }
        DateTimeOffset Changed { get; set; }
    }
}