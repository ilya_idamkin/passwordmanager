﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace PasswordManager.Web.Models.WebApiAuthorizationViewModels
{
    public class LogoutViewModel
    {
        [BindNever]
        public string RequestId { get; set; }
    }
}
