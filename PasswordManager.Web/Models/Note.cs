﻿using PasswordManager.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace PasswordManager.Web.Models
{
    public class Note : INote
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Other { get; set; }
        public DateTimeOffset Changed { get; set; }
        [Required]
        [ForeignKey("AppUser")]
        public string UserId { get; set; }

        public virtual ApplicationUser AppUser { get; set; }
    }
}