﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PasswordManager.Web.Models;

namespace PasswordManager.Web.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            SeedDatabase(context);
            SeedAdmin(roleManager, userManager).Wait();
        }

        public static void SeedDatabase(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();
        }

        public static async Task SeedAdmin(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            var adminRoles = new List<String> { "Admin", "User" };

            foreach (var role in adminRoles)
            {
                if (!await roleManager.RoleExistsAsync(role))
                {
                    await roleManager.CreateAsync(new IdentityRole(role));
                }
            }

            // создаем пользователей
            var email = "admin@gmail.com";
            var admin = await userManager.FindByEmailAsync(email);
            if (admin == null) {
                admin = new ApplicationUser { Email = email, UserName = email, EmailConfirmed = true };
                string password = "123qwe!@#QWE";
                await userManager.CreateAsync(admin, password);
            }
            
            foreach (var role in adminRoles)
            {
                await userManager.AddToRoleAsync(admin, role);
            }
        }
    }
}
