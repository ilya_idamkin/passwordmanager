﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PasswordManager.Mobile.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppUrl_Android))]
namespace PasswordManager.Mobile.Droid
{
    public class AppUrl_Android : IAppUrl
    {
        public string Get()
        {
            return "http://192.168.1.101:3000";
        }
    }
}