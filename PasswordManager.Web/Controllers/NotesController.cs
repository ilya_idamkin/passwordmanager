﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PasswordManager.Web.Data;
using PasswordManager.Web.Models;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using PasswordManager.Core.Extensions;
using PasswordManager.Web.Helpers;

namespace PasswordManager.Web.Controllers
{
    [Authorize(Roles = "User")]
    public class NotesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public NotesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Notes
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Notes.Include(n => n.AppUser).Where(n => n.UserId == _userManager.GetUserId(HttpContext.User));
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Notes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            string cryptoKey = this.RetriveCryptoHash();
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Notes
                .Include(n => n.AppUser)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }
            if (cryptoKey == null)
            {
                return RedirectToAction("VerifyPassword", "Account", new { ReturnUrl = UriHelper.GetDisplayUrl(Re‌​quest) });
            }
            note.Decrypt(cryptoKey);
            return View(note);
        }

        // GET: Notes/Create
        public IActionResult Create()
        {
            if (this.RetriveCryptoHash() == null)
            {
                return RedirectToAction("VerifyPassword", "Account", new { ReturnUrl = UriHelper.GetDisplayUrl(Re‌​quest) });
            }
            this.KeepCryptoHash();
            return View(new Note { UserId = _userManager.GetUserId(HttpContext.User) });
        }

        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Login,Password,Other,Changed,UserId")] Note note)
        {
            string cryptoKey = this.RetriveCryptoHash();
            if (cryptoKey == null)
            {
                return BadRequest();
            }
            if (ModelState.IsValid)
            {
                note.Encrypt(cryptoKey);
                note.Changed = DateTimeOffset.Now;
                _context.Add(note);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(new Note { UserId = _userManager.GetUserId(HttpContext.User) });
        }

        // GET: Notes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            string cryptoKey = this.RetriveCryptoHash();
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Notes.SingleOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }
            if (cryptoKey == null)
            {
                return this.CallVerifyPassword();
            }
            note.Decrypt(cryptoKey);
            this.KeepCryptoHash();
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Login,Password,Other,Changed,UserId")] Note note)
        {
            string cryptoKey = this.RetriveCryptoHash();
            if (id != note.Id || cryptoKey == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    note.Encrypt(cryptoKey);
                    note.Changed = DateTimeOffset.Now;
                    _context.Update(note);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NoteExists(note.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            string cryptoKey = this.RetriveCryptoHash();
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Notes
                .Include(n => n.AppUser)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }
            if (cryptoKey == null)
            {
                return RedirectToAction("VerifyPassword", "Account", new { ReturnUrl = UriHelper.GetDisplayUrl(Re‌​quest) });
            }
            note.Decrypt(cryptoKey);
            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var note = await _context.Notes.SingleOrDefaultAsync(m => m.Id == id);
            _context.Notes.Remove(note);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool NoteExists(int id)
        {
            return _context.Notes.Any(e => e.Id == id);
        }
    }
}
